package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractException;

}
