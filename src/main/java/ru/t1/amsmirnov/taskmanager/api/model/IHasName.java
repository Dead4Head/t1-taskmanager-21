package ru.t1.amsmirnov.taskmanager.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
