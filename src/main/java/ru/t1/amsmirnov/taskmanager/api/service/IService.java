package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
