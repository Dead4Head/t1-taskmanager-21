package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AbstractException;

    User getUser() throws AbstractException;

    void checkRoles(Role[] roles) throws AbstractException;

}
